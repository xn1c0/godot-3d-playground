class_name TActor extends Actor

@export_group("Properties")
@export var jumpHeight: float = 4 ###< jump height (in meters)
@export var jumpTimeToPeak: float = 0.3 ###< jump time to peak
@export var jumpTimeToDescent: float = 0.3 ###< jump time to descent

@onready var jumpVelocity : float = (
	cJumpVelocity(jumpHeight, jumpTimeToPeak)
) ###< jump velocity
@onready var jumpGravity : float = (
	cJumpGravity(jumpHeight, jumpTimeToPeak)
) ###< gravity affecting actor when jumping
@onready var fallGravity : float = (
	cFallGravity(jumpHeight, jumpTimeToDescent)
) ###< gravity affecting actor when falling

## @brief Calculates the jump velocity
## This calculates the rate and direction of an actor jump action
## @return jump_velocity
func cJumpVelocity(height: float, time_to_peak: float) -> float:
	return 2.0 * height / time_to_peak

## @brief Calculates gravity affecting jumps
## This calculates the gravity to be used when an actor is jumping
## @return jump_gravity
func cJumpGravity(height: float, time_to_peak: float) -> float:
	return 2.0 * height / pow(time_to_peak, 2)

## @brief Calculates gravity affecting falls
## This calculates the gravity to be used when an actor is falling
## @return fall_gravity
func cFallGravity(height: float, time_to_descent: float) -> float:
	return 2.0 * height / pow(time_to_descent, 2)

func isOnFloor() -> bool:
	return is_on_floor()

func getGravitationalForce() -> float:
	return jumpGravity if velocity.y > 0 else fallGravity

func getMovementFriction() -> float:
	return 1.0 if isOnFloor() else 0.5

func applyGravitationalForce(delta: float) -> void:
	velocity.y -= getGravitationalForce() * delta

func applyRotation(delta: float) -> void:
	if Vector2(velocity.z, velocity.x).length() > 0:
		rotationDirection = Vector2(velocity.z, velocity.x).angle()
	rotation.y = lerp_angle(rotation.y, rotationDirection, delta * 10)

func isRequestingToJump() -> bool:
	return false

func applyHorizontalVelocity(
	movementDirection: Vector3, 
	delta: float
) -> void:
	return
	#var what: String = "Function '%s' of '%s' has not been implemented."
	#push_warning(what % ["applyHorizontalVelocity", self.name])

func jump(multiplier: float = 1.0) -> void:
	velocity.y = jumpVelocity * multiplier
