class_name E01SChase extends Enemy01State

var isOnLink: bool = false
var linkDetails: Dictionary = {}

func enter(manager: StateManager) -> void:
	print("CHASE")
	_enemy._navigation_agent.link_reached.connect(onLinkReached)

func processPhysics(manager: StateManager, delta: float) -> void:
	
	if isOnLink:
		manager.transitTo(E01SLink.new(_enemy, linkDetails))
	
	_enemy.applyGravitationalForce(delta)
	_enemy._navigation_agent.set_target_position(_enemy._target.position)
	var current_agent_position: Vector3 = _enemy.global_position
	var next_path_position: Vector3 = _enemy._navigation_agent.get_next_path_position()
	var direction: Vector3 = current_agent_position.direction_to(next_path_position)
	_enemy.applyHorizontalVelocity(direction, delta)
	_enemy.move()
	_enemy.applyRotation(delta)
	_enemy.applyEffects()

func onLinkReached(details: Dictionary) -> void:
	isOnLink = true
	linkDetails = details
