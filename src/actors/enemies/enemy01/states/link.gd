class_name E01SLink extends Enemy01State

var _linkDetails: Dictionary = {}
var _destination: Vector3 = Vector3.ZERO

func _init(enemy: Enemy01, linkDetails: Dictionary):
	super(enemy)
	assert(linkDetails)
	_linkDetails = linkDetails

func enter(manager: StateManager) -> void:
	print("LINK")
	print(_linkDetails)
	var x: Vector3 = _linkDetails["link_exit_position"] as Vector3
	_destination = x
	print(x)
	if x.y > _enemy.position.y:
		_enemy.jump()
	else:
		print("POPPO")

func processPhysics(manager: StateManager, delta: float) -> void:
	_enemy.applyGravitationalForce(delta)
	var direction: Vector3 = _enemy.global_position.direction_to(_destination)

	print("DD ", _destination, "   GG ", _enemy.global_position)
	_enemy.applyHorizontalVelocity(direction, delta)
	_enemy.move()
	_enemy.applyRotation(delta)
	_enemy.applyEffects()
	
	if(_destination.distance_to(_enemy.global_position) < 1):
		manager.transitTo(E01SChase.new(_enemy))
