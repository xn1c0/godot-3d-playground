class_name Enemy01State extends BaseState

var _enemy: Enemy01 ###< player reference

## @brief Constructor
## Initialize the PlayerState.
## @param player reference
func _init(enemy: Enemy01) -> void:
	assert(enemy)
	_enemy = enemy
