class_name Enemy01 extends TActor

@export var _target: Node3D = null
@onready var _navigation_agent: NavigationAgent3D = $NA3D

var stateManager: StateManager = null

func _ready() -> void:
	super()
	assert(_target)
	stateManager = StateManager.new(E01SChase.new(self))
	
func _physics_process(delta: float) -> void:
	print(_navigation_agent.is_target_reached())
	stateManager.processPhysics(delta)


func move() -> void:
	move_and_slide()

func applyEffects() -> void:
	if is_on_floor():
		if abs(velocity.x) > 1 or abs(velocity.z) > 1:
			animationPlayer.play("walk", 0.5)
			#particles_trail.emitting = true
			#sound_footsteps.stream_paused = false
		else:
			animationPlayer.play("idle", 0.5)
	else:
		animationPlayer.play("jump", 0.5)

func applyHorizontalVelocity(
	movementDirection: Vector3,
	delta: float
) -> void:
	if movementDirection:
		velocity.x = lerp(velocity.x, movementDirection.x * movementSpeed * delta, getMovementFriction()) 
		velocity.z = lerp(velocity.z, movementDirection.z * movementSpeed * delta, getMovementFriction()) 
		#velocity.x = movementDirection.x * movementSpeed * delta
		#velocity.z = movementDirection.z * movementSpeed * delta
	else:
		velocity.x = move_toward(velocity.x, 0, movementSpeed)
		velocity.z = move_toward(velocity.z, 0, movementSpeed)
