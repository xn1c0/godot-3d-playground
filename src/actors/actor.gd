class_name Actor extends CharacterBody3D

#######################################
### Class variables

@export_group("Properties")
@export var movementSpeed: float = 500 ###< movement speed 
@export var model: Node3D = null ###< actor model

var rotationDirection: float = 0.0 ###< actor front face direction
var animationPlayer: AnimationPlayer = null ###< actor animation player


func _ready() -> void:
	assert(model)
	var ap: AnimationPlayer = model.get_children()[1] as AnimationPlayer 
	assert(ap)
	animationPlayer = ap

##### USeful

func isVelocityOnXAxisPositive() -> bool:
	return velocity.x > 0.0

func isVelocityOnXAxisNegative() -> bool:
	return not isVelocityOnXAxisPositive()

func isVelocityOnYAxisPositive() -> bool:
	return velocity.y > 0.0

func isVelocityOnYAxisNegative() -> bool:
	return not isVelocityOnYAxisPositive()

func isVelocityOnZAxisPositive() -> bool:
	return velocity.z > 0.0

func isVelocityOnZAxisNegative() -> bool:
	return not isVelocityOnZAxisPositive()

########## DEFINED

func resetVelocity() -> void:
	velocity = Vector3.ZERO

# @brief Apply rotation
# Apply rotation to make actor facing the desired point
func applyRotation(delta: float) -> void:
	return
	#push_warning(
		#"Function applyRotation of ", Format.quote(self.name),
		#" has not been implemented."
	#)

# @brief Move the actor
# Actor movement function
func move() -> void:
	return
	#push_warning(
		#"Function move of ", Format.quote(self.name),
		#" has not been implemented."
	#)

# @brief Retrieves actor movement direction
func getMovementDirection() -> Vector3:
	#push_warning(
		#"Function getMovementDirection of ", Format.quote(self.name),
		#" has not been implemented. Returning Vector3.ZERO"
	#)
	return Vector3.ZERO


func applyEffects() -> void:
	return
	#push_warning(
		#"Function applyEffects of ", Format.quote(self.name),
		#" has not been implemented."
	#)
