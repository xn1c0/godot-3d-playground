class_name PlayerState extends BaseState

var _player: Player ###< player reference

## @brief Constructor
## Initialize the PlayerState.
## @param player reference
func _init(player: Player) -> void:
	assert(player)
	_player = player

func notifyChangeOfStatus(what: String) -> void:
	EventBus.emit_signal("PlayerStatusChangedSignal", what)
