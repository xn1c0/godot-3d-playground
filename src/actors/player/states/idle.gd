class_name PSIdle extends PlayerState

func enter(manager: StateManager) -> void:
	notifyChangeOfStatus("Idle")

func processInput(manager: StateManager, event: InputEvent) -> void:
	if _player.isRequestingToJump() and _player.isOnFloor():
		manager.transitTo(PSJump.new(_player))
	if _player.getMovementDirection() != Vector3.ZERO:
		manager.transitTo(PSWalk.new(_player))

func processPhysics(manager: StateManager, delta: float) -> void:
	_player.applyGravitationalForce(delta)
	_player.move()
	_player.applyRotation(delta)
	_player.applyEffects()
	
	if not _player.isOnFloor():
		manager.transitTo(PSFall.new(_player))
