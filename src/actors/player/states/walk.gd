class_name PSWalk extends PlayerState

func enter(manager: StateManager) -> void:
	notifyChangeOfStatus("Walk")

func processInput( manager: StateManager, event: InputEvent) -> void:
	if _player.isRequestingToJump() and _player.isOnFloor():
		return manager.transitTo(PSJump.new(_player))

func processPhysics(manager: StateManager, delta: float) -> void:
	_player.applyGravitationalForce(delta)
	var direction: Vector3 = _player.getMovementDirection()
	_player.applyHorizontalVelocity(direction, delta)
	_player.move()
	_player.applyRotation(delta)
	_player.applyEffects()
	
	if _player.isOnFloor():
		if direction == Vector3.ZERO:
			manager.transitTo(PSIdle.new(_player))
	else:
		return manager.transitTo(PSFall.new(_player))
