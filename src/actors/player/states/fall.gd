class_name PSFall extends PlayerState

var _tJCWaitTime: float
var _tJCCurrent: float = 0.0
var _tJCIsStopped: bool = true

var _tJBWaitTime: float
var _tJBCurrent: float = 0.0
var _tJBIsStopped: bool = true

func _init(
	player: Player,
	tJumpCoyote: float = 0.1,
	tJumpBuffering: float = 0.1
) -> void:
	super(player)
	_tJCWaitTime = tJumpCoyote
	_tJBWaitTime = tJumpBuffering

func processTimers(delta: float) -> void:
	if not _tJCIsStopped:
		_tJCCurrent += delta
		if _tJCCurrent >= _tJCWaitTime:
			_tJCCurrent = 0.0
			_tJCIsStopped = true
	if not _tJBIsStopped:
		_tJBCurrent += delta
		if _tJBCurrent >= _tJBWaitTime:
			_tJBCurrent = 0.0
			_tJBIsStopped = true

func enter(manager: StateManager) -> void:
	notifyChangeOfStatus("Fall")
	_tJCIsStopped = false

func processInput(manager: StateManager, event: InputEvent) -> void:
	if _player.isRequestingToJump():
		_tJBIsStopped = false    
		if not _tJCIsStopped:
			return manager.transitTo(PSJump.new(_player))

func processPhysics(manager: StateManager,delta: float) -> void:
	processTimers(delta)
	_player.applyGravitationalForce(delta)
	var direction: Vector3 = _player.getMovementDirection()
	_player.applyHorizontalVelocity(direction, delta)
	_player.move()
	_player.applyEffects()
	_player.applyRotation(delta)
	
	if _player.isOnFloor() and not _tJBIsStopped:
		return manager.transitTo(PSJump.new(_player))
	
	if _player.isOnFloor():
		if direction == Vector3.ZERO:
			return manager.transitTo(PSIdle.new(_player))
		else:
			return manager.transitTo(PSWalk.new(_player))
