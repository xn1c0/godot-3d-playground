class_name PSJump extends PlayerState

func enter(manager: StateManager) -> void:
	notifyChangeOfStatus("Jump")
	_player.jump()

func processPhysics(manager: StateManager, delta: float) -> void:
	_player.applyGravitationalForce(delta)
	_player.applyHorizontalVelocity(_player.getMovementDirection(), delta)
	_player.move()
	_player.applyRotation(delta)
	_player.applyEffects()
	
	if not _player.isVelocityOnYAxisNegative():
		return manager.transitTo(PSFall.new(_player))
