class_name Player extends TActor

@export_group("Properties")
@export var _viewPoint: Node3D = null ###< player view point

var stateManager: StateManager = null
var _perfectJumpsCounter: int = 0

###############################################################################

func _ready() -> void:
	super()
	assert(_viewPoint)
	stateManager = StateManager.new(PSIdle.new(self))

func _input(event: InputEvent) -> void:
	stateManager.processInput(event)

func _process(delta: float) -> void:
	stateManager.processFrame(delta)

func _physics_process(delta: float) -> void:
	stateManager.processPhysics(delta)
	for i in get_slide_collision_count():
		var c = get_slide_collision(i)
		#if c.get_collider() is RigidBody3D:
			#var x: RigidBody3D = c.get_collider()
			#x.apply_central_impulse(-c.get_normal() * 30.0)


###############################################################################

func addPerfectJump() -> void:
	_perfectJumpsCounter += 1

func resetPerfectJumps() -> void:
	_perfectJumpsCounter = 0

func canDoSuperJump() -> bool:
	if _perfectJumpsCounter == 3:
		print("YESSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS")
		resetPerfectJumps()
		return true
	else:
		return false

###############################################################################

## @brief Move the actor
## Actor movement function
func move() -> void:
	move_and_slide()

## @brief Retrieves actor movement direction
func getMovementDirection() -> Vector3:
	var input_dir_2d: Vector2 = Input.get_vector(
		"gameplay_movement_left", 
		"gameplay_movement_right", 
		"gameplay_movement_forward", 
		"gameplay_movement_backwards"
	)
	var input_dir_3d: Vector3 = Vector3(input_dir_2d.x, 0, input_dir_2d.y)
	return input_dir_3d.rotated(Vector3.UP, _viewPoint.rotation.y).normalized()

func isRequestingToJump() -> bool:
	return Input.is_action_just_pressed("gameplay_action_jump")

func applyEffects() -> void:
	if is_on_floor():
		if abs(velocity.x) > 1 or abs(velocity.z) > 1:
			animationPlayer.play("walk", 0.5)
			#particles_trail.emitting = true
			#sound_footsteps.stream_paused = false
		else:
			animationPlayer.play("idle", 0.5)
	else:
		animationPlayer.play("jump", 0.5)

func applyHorizontalVelocity(
	movementDirection: Vector3,
	delta: float
) -> void:
	if movementDirection:
		velocity.x = lerp(velocity.x, movementDirection.x * movementSpeed * delta, getMovementFriction()) 
		velocity.z = lerp(velocity.z, movementDirection.z * movementSpeed * delta, getMovementFriction()) 
		#velocity.x = movementDirection.x * movementSpeed * delta
		#velocity.z = movementDirection.z * movementSpeed * delta
	else:
		velocity.x = move_toward(velocity.x, 0, movementSpeed)
		velocity.z = move_toward(velocity.z, 0, movementSpeed)
