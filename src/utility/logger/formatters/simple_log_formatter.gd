class_name SimpleLogFormatter extends LogFormatter

func format(record: LogRecord) -> String:
	var placeholder: String = "[{dt}][{lvl}][{tag}]: {msg}"
	var output: String = placeholder.format({
		"dt": record.getDateTime(),
		"lvl": record.getLevelName(),
		"tag": record.getTag(),
		"msg": record.getMessage()
	})
	return output
