class_name FileLogHandler extends LogHandler

var _file: FileAccess = null

func _init(
	formatter: LogFormatter,
	logsDir: String = "logs"
) -> void:
	super(formatter)
	var dirPath: String = "res://%s/" % logsDir
	DirAccess.make_dir_absolute(dirPath)
	var dt: String = Time.get_datetime_string_from_system()
	var filePath: String = dirPath + dt + ".log"
	_file = FileAccess.open(filePath, FileAccess.WRITE)
	assert(_file)

func _notification(what: int) -> void:
	if what == NOTIFICATION_PREDELETE:
		_file.close()

func publish(record: LogRecord) -> void:
	var output: String = _formatter.format(record)
	_file.store_line(output)
