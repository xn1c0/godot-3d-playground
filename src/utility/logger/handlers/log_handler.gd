class_name LogHandler extends RefCounted

var _formatter: LogFormatter

func _init(formatter: LogFormatter):
	assert(formatter)
	_formatter = formatter

func publish(record: LogRecord) -> void:
	return
