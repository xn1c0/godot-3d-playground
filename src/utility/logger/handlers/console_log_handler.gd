class_name ConsoleLogHandler extends LogHandler

func publish(record: LogRecord) -> void:
	var output: String = _formatter.format(record)
	print(output)
