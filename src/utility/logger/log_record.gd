class_name LogRecord extends RefCounted

enum Level {
	All = -9223372036854775808, ## Captures everything
	Debug = 100, ## General Developer Information
	Info = 200, ## General Information
	Warning = 300, ## Potential Problem
	Error = 1000, ## Represents serious failure
	Off = 9223372036854775807 ##  Turns off the logging
}

var _dateTime: String
var _message: String
var _tag: String
var _level: Level

func _init(
	message: String, 
	tag: String,
	level: Level
) -> void:
	assert(message)
	assert(tag)
	_dateTime = Time.get_datetime_string_from_system()
	_message = message
	_tag = tag
	_level = level

func getDateTime() -> String:
	return _dateTime

func getMessage() -> String:
	return _message

func getTag() -> String:
	return _tag

func getLevel() -> Level:
	return _level

func getLevelName() -> String:
	var result: String = ""
	match _level:
		Level.All: result = "All"
		Level.Debug: result = "Debug"
		Level.Info: result = "Info"
		Level.Warning: result = "Warning"
		Level.Error: result = "Error"
		Level.Off: result = "Off"
	return result
