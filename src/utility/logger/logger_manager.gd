class_name LoggerManager extends Node

var _level : LogRecord.Level

var _enabled : bool
var _default_output_enabled : bool

var _handlers: Array[LogHandler]

func _enter_tree():
	_enabled = true
	_level = LogRecord.Level.Info
	addHandler(ConsoleLogHandler.new(SimpleLogFormatter.new()))
	addHandler(FileLogHandler.new(SimpleLogFormatter.new()))

func addHandler(handler: LogHandler) -> void:
	assert(handler)
	_handlers.push_back(handler)

func isLevelAccepted(record: LogRecord) -> bool:
	return _level >= record.getLevel()

func log(message: String, tag: String, level: LogRecord.Level) -> void:
	var record: LogRecord = LogRecord.new(message, tag, level)
	for handler in _handlers:
		if _enabled and isLevelAccepted(record):
			handler.publish(record)

func logDebug(
	message: String, 
	tag: String = "Main",
	level: LogRecord.Level = LogRecord.Level.Debug
) -> void:
	self.log(message, tag, level)

func logInfo(
	message: String, 
	tag: String = "Main",
	level: LogRecord.Level = LogRecord.Level.Info
) -> void:
	self.log(message, tag, level)

func logWarning(
	message: String, 
	tag: String = "Main",
	level: LogRecord.Level = LogRecord.Level.Warning
) -> void:
	self.log(message, tag, level)

func logError(
	message: String, 
	tag: String = "Main",
	level: LogRecord.Level = LogRecord.Level.Error
) -> void:
	self.log(message, tag, level)
