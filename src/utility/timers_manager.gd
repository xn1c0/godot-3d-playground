class_name TimersManager extends Node

var _tTable: Dictionary 

func _ready():
	for child in get_children():
		var timer: Timer = child as Timer
		assert(timer)
		addTimerByName(timer)

func addTimerByName(timer: Timer) -> void:
	assert(timer)
	_tTable[timer.name] = timer

func getTimer(name: String) -> Timer:
	assert(name)
	return _tTable.get(name)
