class_name BaseState extends RefCounted

## @brief Entering this state
## Function called when StateManager has left an old state, and is entering
## this one at the end of the transitTo function
## @param manager context using the state
## @see transitTo in state_manager.gd
func enter(manager: StateManager) -> void:
	return

## @brief Exiting this state
## Function called when StateManager is leaving this state 
## to transit to a new one.
## @param manager context using the state
## @see transitTo in state_manager.gd
func exit(manager: StateManager) -> void:
	return

## @brief Pass through function
## Pass through functions for the StateManager to call.
## @param manager context using the state
## @param event input to be handled
func processInput(manager: StateManager, event: InputEvent) -> void:
	return

## @brief Pass through function
## Pass through functions for the StateManager to call.
## @param manager context using the state
## @param delta amount of time elapsed during one frame
func processFrame(manager: StateManager, delta: float) -> void:
	return

## @brief Pass through function
## Pass through functions for the StateManager to call.
## @param manager context using the state
## @param delta amount of time elapsed during one frame
func processPhysics(manager: StateManager, delta: float) -> void:
	return
