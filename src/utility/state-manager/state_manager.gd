class_name StateManager extends RefCounted

var _state: BaseState ###< current state

## @brief Constructor
## Initialize the StateManager
## @param startingState initial state
func _init(startingState: BaseState) -> void:
	assert(startingState)
	_state = startingState
	_state.enter(self)

## @brief Transitioning to a new state
## Performs the transition form the current state to the new one.
## @param state the new state to transition to
func transitTo(state: BaseState) -> void:
	assert(state)
	_state.exit(self)
	_state = state
	_state.enter(self)

## @brief Pass through function
## Pass through functions called by a client.
## @param event input to be handled
func processInput(event: InputEvent) -> void:
	_state.processInput(self, event)

## @brief Pass through function
## Pass through functions called by a client.
## @param delta amount of time elapsed during one frame
func processFrame(delta: float) -> void:
	_state.processFrame(self, delta)

## @brief Pass through function
## Pass through functions called by a client.
## @param delta amount of time elapsed during one frame
func processPhysics(delta: float) -> void:
	_state.processPhysics(self, delta)
