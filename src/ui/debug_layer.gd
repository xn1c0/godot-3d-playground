extends CanvasLayer

@onready var _fps: Label = (
	$DebugParametersMargin/DebugParameters/FramesPerSecond/Value
)

@onready var _playerState: Label = (
	$DebugParametersMargin/DebugParameters/PlayerState/Value
)

func _ready() -> void:
	EventBus.connect("PlayerStatusChangedSignal", onPlayerStatusChangedSignal)

func _physics_process(delta) -> void:
	_fps.set_text(str(Engine.get_frames_per_second()))

func onPlayerStatusChangedSignal(what : String) -> void:
	_playerState.set_text(what)
