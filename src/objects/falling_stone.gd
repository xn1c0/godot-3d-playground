extends Node3D

@onready var player_detector: Area3D = $PlayerDetector/Area3D
@onready var stone: RigidBody3D = $RigidBody3D

# Called when the node enters the scene tree for the first time.
func _ready():
	player_detector.body_entered.connect(onBodyEntered)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	return

func onBodyEntered(body: Node3D) -> void:
	if body is Player:
		N7Logger.logDebug("Player detected below %s." % self, "Traps")
		stone.freeze = false
		player_detector.queue_free()
