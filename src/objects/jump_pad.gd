extends Node3D

@onready var player_detector: Area3D = $Area3D

# Called when the node enters the scene tree for the first time.
func _ready():
	player_detector.body_entered.connect(onBodyEntered)

func onBodyEntered(body: Node3D) -> void:
	if body is Player:
		N7Logger.logDebug("Player detected on %s." % self, "JumpPads")
		var x: Player = body as Player
		x.jump()
