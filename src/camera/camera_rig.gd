extends Node3D

@export var actor: Actor

@export var camera: Camera3D
@onready var focus = $focus

@export var stage_width: float = 10
@export var stage_depth: float = 30

func _ready():
	assert(actor != null)

func _physics_process(delta):
	position = lerp(position, actor.position, delta * 10.0)
	position.x = clampf(position.x, -stage_width/2, stage_width/2)
	position.z = clampf(position.z, -stage_depth/2, stage_depth/2)
	
	camera.look_at(((actor.position+position)/2)+Vector3.UP,Vector3.UP)
	
	#focus.global_position = ((actor.position+position)/2)+Vector3.UP
