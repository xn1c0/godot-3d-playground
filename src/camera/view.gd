extends Node3D

@export_group("Properties")
@export var target: Node3D

@export_group("Zoom")
@export var zoomMinimum = 16
@export var zoomMaximum = 4
@export var zoomSpeed = 10

@export_group("Rotation")
@export var rotationSpeed = 120

var cameraRotation: Vector3 = Vector3.ZERO
var zoom: int = 10

var camera: Camera3D = null

func _ready():
	assert(get_child_count() == 1)
	camera = get_child(0) as Camera3D
	assert(camera)
	cameraRotation = rotation_degrees # Initial rotation

func _physics_process(delta: float):
	
	# Set position and rotation to targets
	position = position.lerp(target.position, delta * 4)
	rotation_degrees = rotation_degrees.lerp(cameraRotation, delta * 6)
	camera.position = camera.position.lerp(Vector3(0, 0, zoom), 8 * delta)
	
	handle_input(delta)

# Handle input

func handle_input(delta):
	
	# Rotation
	
	var input := Vector3.ZERO
	
	input.y = Input.get_axis("camera_right", "camera_left")
	input.x = Input.get_axis("camera_down", "camera_up")
	
	cameraRotation += input.limit_length(1.0) * rotationSpeed * delta
	cameraRotation.x = clamp(cameraRotation.x, -80, -10)
	
	## Zooming
	#
	#zoom += Input.get_axis("zoom_in", "zoom_out") * zoomSpeed * delta
	#zoom = clamp(zoom, zoomMaximum, zoomMinimum)
